package com.mindfio.user_app.network

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {

    var token : String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiMzIzZHNkc2RzZHNkc2Rrc2RzZGQiLCJlbWFpbCI6InNhaWZAdGhlZ2hhYXppLmNvbSIsImF2YXRhciI6Imh0dHBzOi8vbWVkaWEubGljZG4uY29tL2Rtcy9pbWFnZS9DNTYwM0FRRlN4ZWU4ZE9Ma21RL3Byb2ZpbGUtZGlzcGxheXBob3RvLXNocmlua184MDBfODAwLzA_ZT0xNTc2MTA4ODAwJnY9YmV0YSZ0PVNEdFNNTGRfcllza3pmYTJRMEZNVVdvZkg5YnFnSk9zbms2aEt2YXJLRm8iLCJuYW1lIjoiU2FpZiBTaGFpa2giLCJhZGRyZXNzIjoiMiBTdGF0ZSBzdC4iLCJwaG9uZSI6IisxNDY5NzM0Nzg5NiJ9LCJpYXQiOjE1OTI4Nzc4ODQsImV4cCI6MTU5MzQ4MjY4NH0.00_ugF28Mh9UHM3vEYwrBzIj4cejJYXPVw9Nzf1UPSM";

    fun Token(token: String ) {
        this.token = token;
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if(request.header("No-Authentication")==null){
            //val token = getTokenFromSharedPreference();
            //or use Token Function
            if(!token.isNullOrEmpty())
            {
                val finalToken =  "Bearer "+token
                request = request.newBuilder()
                    .addHeader("Authorization",finalToken)
                    .build()
            }

        }

        return chain.proceed(request)
    }

}