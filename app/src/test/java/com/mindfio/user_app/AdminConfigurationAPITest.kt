package com.mindfio.user_app

import android.util.Log
import com.mindfio.user_app.network.api.admin_configuration.AdminConfigurationAPI
import com.mindfio.user_app.network.response.AdminConfigurationResponse
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class AdminConfigurationAPITest {
    @Test
    fun `can get admin configuration` () {
        System.out.println("Test1")

        runBlocking {
            val response = doNetworkRequestSync()
            System.out.println("Test2")
            assertTrue(response.msg == "Success")
        }
    }

    suspend fun doNetworkRequestSync(): AdminConfigurationResponse = suspendCoroutine { cont ->
        AdminConfigurationAPI.getAdminConfigurations().subscribe(
            { response ->
                System.out.println(response)
                cont.resume(response)
            }, { throwable ->
                System.out.println(throwable)
                cont.resumeWithException(throwable)
            })
    }
}