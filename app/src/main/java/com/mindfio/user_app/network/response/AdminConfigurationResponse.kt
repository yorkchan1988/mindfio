package com.mindfio.user_app.network.response

import com.google.gson.annotations.SerializedName

data class AdminConfigurationResponse (

    @SerializedName("msg") val msg : String,
    @SerializedName("data") val data : List<String>
)