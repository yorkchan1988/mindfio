package com.mindfio.user_app.repository

import androidx.lifecycle.LiveData
import com.mindfio.user_app.model.Word
import com.mindfio.user_app.model.dao.WordDao

class WordRepository(private val wordDao: WordDao) {
    val allWords: LiveData<List<Word>> = wordDao.getAlphabetizedWords()

    suspend fun insert(word: Word) {
        wordDao.insert(word)
    }
}