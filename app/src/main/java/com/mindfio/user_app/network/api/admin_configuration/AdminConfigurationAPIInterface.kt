package com.mindfio.user_app.network.api.admin_configuration

import com.mindfio.user_app.network.response.AdminConfigurationResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Header

interface AdminConfigurationAPIInterface {
    @GET("admin-configuration")
    fun getAdminConfigurations() : Observable<AdminConfigurationResponse>
}