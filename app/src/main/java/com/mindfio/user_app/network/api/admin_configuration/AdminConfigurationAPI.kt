package com.mindfio.user_app.network.api.admin_configuration

import com.mindfio.user_app.network.RetrofitService

object AdminConfigurationAPI {
    private var api : AdminConfigurationAPIInterface = RetrofitService.createService(AdminConfigurationAPIInterface::class.java)

    fun getAdminConfigurations() = api.getAdminConfigurations()
}